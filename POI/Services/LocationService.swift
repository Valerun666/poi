//
//  LocationService.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

import CoreLocation

class LocationService: NSObject  {
  var lastLocation: CLLocation?
  var manager: CLLocationManager!
  
  override init() {
    manager = CLLocationManager()
    super.init()
    
    manager.delegate = self
    setBestAccuracy()
    manager.requestAlwaysAuthorization()
    manager.requestWhenInUseAuthorization()
  }
  
  func setBestAccuracy() {
    manager.distanceFilter = kCLHeadingFilterNone
    manager.desiredAccuracy = kCLLocationAccuracyBest
  }
  
  func startService() {
    manager.startUpdatingLocation()
  }
  
  func stopService() {
    manager.stopUpdatingLocation()
  }
}

extension LocationService : CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    lastLocation = locations.last
    NotificationCenter.default.post(name: Notification.Name(rawValue: K.Notification.LocationUpdated), object: nil, userInfo: ["location": lastLocation!])
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    print("*** LocationStatus changed \(status)")
    if status == .authorizedWhenInUse || status == .authorizedAlways {
      print("*** Sending didChangeAuthorization notification from LocationService")
      manager.startUpdatingLocation()
      NotificationCenter.default.post(name: Notification.Name(rawValue: K.Notification.LocationChangedAuthor), object: nil, userInfo: nil)
    }
  }
}
