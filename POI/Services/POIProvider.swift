//
//  PoiProvider.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

protocol POIProvicerProtocol {
  func providePOIList(url: URL, result: @escaping (_ poiList: [POI]) -> Void, failure: @escaping(_ description: String) -> Void)
}

class POIProvider: POIProvicerProtocol {
  func providePOIList(url: URL, result: @escaping ([POI]) -> Void, failure: @escaping (String) -> Void) {
    Alamofire.request(url).responseArray(keyPath: "results.items") { (response: DataResponse<[POI]>) in
      guard response.result.error == nil else {
        print("*** PoiProvider Data Loading *** \(String(describing: response.result.error))")
        failure((response.result.error?.localizedDescription)!)
        return
      }
      
      let poiList = response.result.value
      
      if let poiList = poiList {
        result(poiList)
      } else {
        print("*** PoiProvider *** Something whent wrong during data loading")
        failure("Something whent wrong during data loading")
      }
    }
  }
}
