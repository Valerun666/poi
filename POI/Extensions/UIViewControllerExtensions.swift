//
//  UIViewControllerExtensions.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

extension UIViewController {
  func openModule(segueIdentifier: String, configurationBlock: @escaping (_ viewInput: UIViewController)->Void) {
    let segueInfo = SegueInfo(confBlock: configurationBlock)
    self.performSegue(withIdentifier: segueIdentifier, sender: segueInfo)
  }
}
