//
//  CLLocationCoordinatesExtensions.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//


import CoreLocation

extension CLLocationCoordinate2D {
  func toString() -> String {
    return "\(latitude),\(longitude)"
  }
}
