//
//  Constants.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

struct K {
  static let URL = "https://places.cit.api.here.com/places/v1/discover/around?at={COORDS}&Accept-Language=en-us&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg"
  static let PlcaeholderURL = "http://bit.ly/2CmXDTv"
  static let Delta = 0.009
  
  struct Notification {
    static let LocationUpdated = "kUpdatedLocationNotif"
    static let LocationChangedAuthor = "kLocationChangedAuthorizationNotif"
  }
  
  struct ViewIdentifier {
    static let POI = "POITableViewCell"
    static let Annotation = "AnntotionView"
  }
}
