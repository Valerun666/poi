//
//  POI.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreLocation
import MapKit

class POI: NSObject, Mappable {
  var id = ""
  var name = ""
  var position = [Double]()
  var iconURL = ""
  var vicinity = ""
//  var coordinates: CLLocationCoordinate2D = {
//    return CLLocationCoordinate2D(latitude: self.position[0], longitude: self.position[1])
//  }
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    id <- map["id"]
    name <- map["title"]
    position <- map["position"]
    iconURL <- map["icon"]
    vicinity <- map["vicinity"]
    vicinity = vicinity.replacingOccurrences(of: "<br/>", with: ", ")
  }
  
  override var description: String {
    return "\(String(describing: name)), coordintaes: \(position), vicinity: \(vicinity)"
  }
}

extension POI: MKAnnotation {
  var mapItem: MKMapItem {
    let placemark = MKPlacemark(coordinate: coordinate)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = name
    
    return mapItem
  }
  
  var title: String? {
    return "\(name) \(position[0]),\(position[1])"
  }
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: self.position[0], longitude: self.position[1])
  }
}

