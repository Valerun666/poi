//
//  SegueInfo.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class SegueInfo {
  var configurationBlock: (_ viewInput: UIViewController)->Void
  
  init(confBlock:@escaping (_ viewInput: UIViewController)->Void) {
    self.configurationBlock = confBlock
  }
}
