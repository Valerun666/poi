//
//  POITableViewCell.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class POITableViewCell: UITableViewCell {
  @IBOutlet weak var thumbnailView: UIImageView!
  @IBOutlet weak var titleLbl: UILabel!
  @IBOutlet weak var addressLbl: UILabel!
  @IBOutlet weak var coordinatesLbl: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }
}
