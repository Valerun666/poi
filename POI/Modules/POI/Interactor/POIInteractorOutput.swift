//
//  POIPOIInteractorOutput.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import Foundation

protocol POIInteractorOutput: class {
  func updatePOIList(data: [POI])
  func errorOccured(title: String, description: String)
}
