//
//  POIPOIInteractor.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import CoreLocation

class POIInteractor {

  weak var output: POIInteractorOutput!
  var locationService: LocationService
  var poiListProvider: POIProvicerProtocol
  var currentLocation: CLLocation?
  
  init(locationService: LocationService, poiListProvider: POIProvicerProtocol) {
    self.locationService = locationService
    self.poiListProvider = poiListProvider
    
    subscribeForNotifications()
    self.locationService.startService()
  }
  
  func subscribeForNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(updateUserLocation(notification:)), name: Notification.Name(rawValue: K.Notification.LocationUpdated), object: nil)
  }
  
  @objc func updateUserLocation(notification: Notification) {
    guard let userInfo = notification.userInfo, let location = userInfo["location"] as? CLLocation else {
      return
    }
    
    currentLocation = location
    loadPOIForCurrentLocation(coordinates: currentLocation!.coordinate.toString())
  }
  
  fileprivate func loadPOIForCurrentLocation(coordinates: String) {
    let urlString = K.URL.replacingOccurrences(of: "{COORDS}", with: coordinates)
    
    guard let url = URL(string: urlString) else {
      print(urlString)
      print("*** POIInteractor *** incorrect URL for pois to load")
      return
    }
    
    poiListProvider.providePOIList(url: url, result: { [weak self] (poiList: [POI]) in
      print(poiList)
      guard let strongSelf = self else {
        print("*** POIInteractor *** self is nil")
        return
      }
      
      strongSelf.locationService.stopService()
      strongSelf.output.updatePOIList(data: poiList)
    }) { [weak self] (error: String) in
      guard let strongSelf = self else {
        print("*** POIInteractor *** self is nil")
        return
      }
      
      strongSelf.output.errorOccured(title: "Error", description: error)
    }
  }
}

extension POIInteractor: POIInteractorInput {
  
}
