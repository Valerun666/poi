//
//  POIDataManager.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import AlamofireImage

class POIDataDisplayManager: NSObject {
  var presenter: POIViewOutput!
  
  init(presenter: POIViewOutput) {
    self.presenter = presenter
  }
}

extension POIDataDisplayManager: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    presenter.showPOIDetails(at: indexPath.row)
  }
}

extension POIDataDisplayManager: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.getPOICount()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: K.ViewIdentifier.POI, for: indexPath) as? POITableViewCell else {
      fatalError("*** Misconfigured cell POITableViewCell!")
    }
    
    cell.thumbnailView.af_setImage(withURL: presenter.getURLForPOI(at: indexPath.row))
    cell.titleLbl.text = presenter.getPOIName(at: indexPath.row)
    cell.addressLbl.text = presenter.getPOIAddress(at: indexPath.row)
    cell.coordinatesLbl.text = presenter.getPOICoordinates(at: indexPath.row)
    
    return cell
  }
}
