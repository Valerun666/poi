//
//  POIPOIViewInput.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

protocol POIViewInput: class {
  func setupInitialState()
  func showActivityIndicator()
  func hideActivityIndicator()
  func reloadData()
  func displayAlert(title: String, description: String)
}
