//
//  POIPOIViewOutput.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit

protocol POIViewOutput {
  func viewIsReady()
  func getPOICount() -> Int
  func showPOIDetails(at: Int)
  func getURLForPOI(at: Int) -> URL
  func getPOIName(at: Int) -> String
  func getPOICoordinates(at: Int) -> String
  func getPOIAddress(at: Int) -> String
}
