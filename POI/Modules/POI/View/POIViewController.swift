//
//  POIPOIViewController.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit

class POIViewController: UIViewController {
  var poiDataManager: POIDataDisplayManager!
  var output: POIViewOutput!
  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupDelegates()
    output.viewIsReady()
  }
  
  func setupDelegates() {
    tableView.delegate = poiDataManager
    tableView.dataSource = poiDataManager
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == POIRouter.mapSegue {
      let configurationHolder = segue.destination as! MapViewController
      (sender as! SegueInfo).configurationBlock(configurationHolder)
    }
  }
}

extension POIViewController: POIViewInput {
  func displayAlert(title: String, description: String) {
    let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(defaultAction)
    
    present(alertController, animated: true, completion: nil)
  }
  
  func showActivityIndicator() {
    tableView.isHidden = true
    activityIndicator.startAnimating()
    activityIndicator.isHidden = false
  }
  
  func hideActivityIndicator() {
    activityIndicator.stopAnimating()
    activityIndicator.isHidden = true
    tableView.isHidden = false
  }
  
  func reloadData() {
    tableView.reloadData()
  }
  
  // MARK: POIViewInput
  func setupInitialState() {
    showActivityIndicator()
  }
}
