//
//  POIPOIInitializer.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit

class POIModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var poiViewController: POIViewController!

    override func awakeFromNib() {

        let configurator = POIModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: poiViewController)
    }

}
