//
//  POIPOIConfigurator.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit

class POIModuleConfigurator {

  func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
    
    if let viewController = viewInput as? POIViewController {
      configure(viewController: viewController)
    }
  }

  private func configure(viewController: POIViewController) {

    let router = POIRouter(transitionHandler: viewController)

    let presenter = POIPresenter()
    presenter.view = viewController
    presenter.router = router
    
    let dataManager = POIDataDisplayManager(presenter: presenter)

    let interactor = POIInteractor(locationService: LocationService(), poiListProvider: POIProvider())
    interactor.output = presenter

    presenter.interactor = interactor
    viewController.output = presenter
    viewController.poiDataManager = dataManager
  }

}
