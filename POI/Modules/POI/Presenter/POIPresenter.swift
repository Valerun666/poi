//
//  POIPOIPresenter.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import CoreLocation

class POIPresenter {

  weak var view: POIViewInput!
  var interactor: POIInteractorInput!
  var router: POIRouterInput!
  var poiList = [POI]()
  
  fileprivate func prepareCoordinates(list: [Double]) -> CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: list[0], longitude: list[1])
  }
}

extension POIPresenter: POIModuleInput {
  
}

extension POIPresenter: POIViewOutput {  
  func viewIsReady() {
    if poiList.count == 0 {
      view.setupInitialState()
    } else {
      view.hideActivityIndicator()
      view.reloadData()
    }
  }
  
  func showPOIDetails(at: Int) {
    router.openMapModule(poi: poiList[at])
  }
  
  func getPOICount() -> Int {
    return poiList.count
  }
  
  func getPOIName(at: Int) -> String {
    return poiList[at].name
  }
  
  func getURLForPOI(at: Int) -> URL {
    guard let url = URL(string: poiList[at].iconURL) else {
      return URL(string:K.PlcaeholderURL)!
    }
    
    return url
  }
  
  func getPOICoordinates(at: Int) -> String {
    return "lat: \(poiList[at].position[0]), lng: \(poiList[at].position[1])"
  }
  
  func getPOIAddress(at: Int) -> String {
    return poiList[at].vicinity
  }
}

extension POIPresenter: POIInteractorOutput {
  func errorOccured(title: String, description: String) {
    view.displayAlert(title: title, description: description)
  }
  
  func updatePOIList(data: [POI]) {
    self.poiList = data
    view.hideActivityIndicator()
    view.reloadData()
  }
}
