//
//  POIPOIRouterInput.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import Foundation
import CoreLocation

protocol POIRouterInput {
  func openMapModule(poi: POI)
}
