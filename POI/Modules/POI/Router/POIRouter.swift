//
//  POIPOIRouter.swift
//  POI
//
//  Created by Valerii Teptiuk on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import CoreLocation

class POIRouter {
  static let mapSegue = "MapSegue"
  
  var transitionHandler: POIViewController
  
  init(transitionHandler: POIViewController) {
    self.transitionHandler = transitionHandler
  }
}

extension POIRouter: POIRouterInput {
  func openMapModule(poi: POI) {
    transitionHandler.openModule(segueIdentifier: POIRouter.mapSegue) { (controller: UIViewController) in
      let mapModuleInitializer = MapModuleInitializer()
      mapModuleInitializer.initializaeMapModule(poi: poi, viewInput: controller as! MapViewController)
    }
  }
}
