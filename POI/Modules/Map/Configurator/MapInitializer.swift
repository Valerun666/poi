//
//  MapMapInitializer.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import CoreLocation

class MapModuleInitializer: NSObject {

  func initializaeMapModule(poi: POI, viewInput: MapViewController) {
    let configurator = MapModuleConfigurator()
    configurator.configureModuleForViewInput(poi: poi, viewInput: viewInput)
  }
}
