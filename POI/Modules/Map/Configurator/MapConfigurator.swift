//
//  MapMapConfigurator.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import CoreLocation

class MapModuleConfigurator {

  func configureModuleForViewInput<UIViewController>(poi: POI, viewInput: UIViewController) {

    if let viewController = viewInput as? MapViewController {
      configure(poi: poi, viewController: viewController)
    }
  }

  private func configure(poi: POI, viewController: MapViewController) {

    let router = MapRouter()

    let presenter = MapPresenter(poi: poi)
    presenter.view = viewController
    presenter.router = router
    
    let displayManager = MapDataDisplayManager(presenter: presenter)
    viewController.displayManager = displayManager
    
    let interactor = MapInteractor()
    interactor.output = presenter

    presenter.interactor = interactor
    viewController.output = presenter
  }

}
