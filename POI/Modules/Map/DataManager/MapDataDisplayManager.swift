//
//  MapDataDisplayManager.swift
//  POI
//
//  Created by Valerii Teptiuk on 2/27/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import MapKit

class MapDataDisplayManager: NSObject {
  var presenter: MapViewOutput!
  
  init(presenter: MapViewOutput) {
    self.presenter = presenter
    super.init()
  }
}

extension MapDataDisplayManager: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if annotation is POI {
      var view: MKMarkerAnnotationView

      if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: K.ViewIdentifier.Annotation)
        as? MKMarkerAnnotationView {
        dequeuedView.annotation = annotation
        view = dequeuedView
      } else {
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: K.ViewIdentifier.Annotation)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
      }
      
      return view
    }
    
    return nil
  }
}
