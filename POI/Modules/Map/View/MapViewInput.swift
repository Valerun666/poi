//
//  MapMapViewInput.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

protocol MapViewInput: class {
  func setupInitialState()
  func setupMapData(poi: POI)
  func setupTitle(title: String)
}
