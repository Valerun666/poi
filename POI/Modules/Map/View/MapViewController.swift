//
//  MapMapViewController.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
  @IBOutlet weak var mapView: MKMapView!
  
  var output: MapViewOutput!
  var displayManager: MapDataDisplayManager!
  
  // MARK: Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    output.viewIsReady()
    
    mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: K.ViewIdentifier.Annotation)
    mapView.delegate = displayManager
  }


  // MARK: MapViewInput
  func setupInitialState() {
  }
  
  fileprivate func zoomTo(coordinate: CLLocationCoordinate2D) {
    let span = MKCoordinateSpan(latitudeDelta: K.Delta, longitudeDelta: K.Delta)
    let region = MKCoordinateRegion(center: coordinate, span: span)
    
    mapView.setRegion(region, animated: true)
  }
}

extension MapViewController: MapViewInput {
  func setupMapData(poi: POI) {
    mapView.addAnnotation(poi)
    zoomTo(coordinate: poi.coordinate)
  }
  
  func setupTitle(title: String) {
    self.title = title
  }
}
