//
//  MapMapViewOutput.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

protocol MapViewOutput {

    /**
        @author Valerii Teptiuk
        Notify presenter that view is ready
    */

    func viewIsReady()
}
