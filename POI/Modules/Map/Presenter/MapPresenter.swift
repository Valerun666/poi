//
//  MapMapPresenter.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import UIKit
import CoreLocation

class MapPresenter {

  weak var view: MapViewInput!
  var interactor: MapInteractorInput!
  var router: MapRouterInput!
  var poi: POI!
  
  init(poi: POI) {
    self.poi = poi
  }

  func viewIsReady() {
    view.setupTitle(title: poi.name)
    view.setupMapData(poi: poi)
  }
}

extension MapPresenter: MapModuleInput {
  
}

extension MapPresenter: MapViewOutput {
  
}

extension MapPresenter: MapInteractorOutput {
  
}
