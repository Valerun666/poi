//
//  MapMapPresenterTests.swift
//  POI
//
//  Created by Valerii Teptiuk on 27/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import XCTest

class MapPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: MapInteractorInput {

    }

    class MockRouter: MapRouterInput {

    }

    class MockViewController: MapViewInput {

        func setupInitialState() {

        }
    }
}
