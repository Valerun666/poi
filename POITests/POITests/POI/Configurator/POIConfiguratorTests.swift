//
//  POIPOIConfiguratorTests.swift
//  POI
//
//  Created by Valerun666 on 26/02/2018.
//  Copyright © 2018 ValeriiTeptiuk. All rights reserved.
//

import XCTest

class POIModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = POIViewControllerMock()
        let configurator = POIModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "POIViewController is nil after configuration")
        XCTAssertTrue(viewController.output is POIPresenter, "output is not POIPresenter")

        let presenter: POIPresenter = viewController.output as! POIPresenter
        XCTAssertNotNil(presenter.view, "view in POIPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in POIPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is POIRouter, "router is not POIRouter")

        let interactor: POIInteractor = presenter.interactor as! POIInteractor
        XCTAssertNotNil(interactor.output, "output in POIInteractor is nil after configuration")
    }

    class POIViewControllerMock: POIViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
